package com.example.darren.insightapp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {


    DBManager db = new DBManager(this);

    EditText email;
    EditText password;
    Button loginBtn;
    TextView CreateAccount;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);


        // Hides the action bar that would usual be at the top of the layout
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }


        CreateAccount = (TextView) findViewById(R.id.CreateAccount);

        // Two input boxes - take in user name and password
        email = (EditText) findViewById(R.id.userName);
        password = (EditText) findViewById(R.id.password);
        // Login Button
        loginBtn = (Button) findViewById(R.id.loginBtn);


        // Checking if button was clicked...
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.open();

                // Checking if the username and password is correct - if so toast message and go to next activity
                if (db.getUser(email.getText().toString(), password.getText().toString())) {
                    Toast.makeText(getApplicationContext(),"Successful Login", Toast.LENGTH_SHORT).show();
                    db.close();

                    Intent main = new Intent(Login.this, MainMenu.class);
                    startActivity(main);
                }
                // otherwise - toast saying error !
                else {
                    Toast.makeText(getApplicationContext(), "No matching username and password found",Toast.LENGTH_LONG).show();
                }
            }
        });

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    db.open();

                    // Checking if the username and password is correct - if so toast message and go to next activity
                    if (db.getUser(email.getText().toString(), password.getText().toString())) {

                        Toast.makeText(getApplicationContext(),
                                "Successful Login", Toast.LENGTH_SHORT).show();

                        db.close();

                        Intent main = new Intent(Login.this, MainMenu.class);
                        startActivity(main);

                    }
                    // otherwise - toast sayin error !
                    else {
                        Toast.makeText(getApplicationContext(), "No matching username and password found",Toast.LENGTH_LONG).show();
                    }
                    handled = true;
                }
                return handled;
            }
        });

        CreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(Login.this, Register.class);
                startActivity(main);
            }
        });

    }
}
