package com.example.darren.insightapp;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

public class Register extends Activity {

	
	DBManager db = new DBManager(this);
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);

        // Hides the action bar that would usual be at the top of the layout
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
		
		final EditText firstname = (EditText)findViewById(R.id.firstname);
		final EditText surname = (EditText)findViewById(R.id.surname);
		final EditText address = (EditText)findViewById(R.id.address);
		final EditText email = (EditText)findViewById(R.id.email);
		final EditText password = (EditText)findViewById(R.id.password);

		final Button btn = (Button)findViewById(R.id.submitBtn);

		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

                Calendar rightNow = Calendar.getInstance();
                String Date = "" + rightNow.get(Calendar.DAY_OF_MONTH) + "/" + (rightNow.get(Calendar.MONTH)+1) + "/" + rightNow.get(Calendar.YEAR);

                db.open(); 				// Open Database
				db.insertUser("" + firstname.getText(), "" +surname.getText(), "" +Date, "" +address.getText(), "" +email.getText(), "" +password.getText());
				db.close();				// Close Database
				finish();
			}
		});
	}
}
